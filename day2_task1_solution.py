## This script has bugs! Save a new copy and try and fix the bugs in it.

import iris 
import iris.quickplot as qplt ##BUG1: this line is missing
import matplotlib.pyplot as plt
import numpy as np
import iris.coord_categorisation # allows year/month/hour auxillary coordinates to be added

## BUG2: reading in multiple files with load_cube
cube_pres = iris.load_cube('./data_all/pr_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')
cube_fut = iris.load_cube('./data_all/pr_Amon_HadGEM2-ES_rcp85_r1i1p1_203012-205511.nc')

## change units from kg m-2 s-1 to mm hr-1
cube_pres.data = cube_pres.data *3600.0
cube_pres.units = "mm hr-1"
cube_fut.data = cube_fut.data *3600.0
cube_fut.units = "mm hr-1"


## select East Africa
## set longitude range
min_lon = 20.0; max_lon = 52.0
def lon_range(cell):
	return min_lon <= cell <= max_lon
min_lat = -10.0; max_lat = 20.0
def lat_range(cell):
	return min_lat <= cell <= max_lat
## BUG3: This constraint is named differently.
constraint_EA = iris.Constraint(longitude=lon_range, latitude=lat_range)
## extract EA region
cube_pres_EA = cube_pres.extract(constraint_EA)
cube_fut_EA = cube_fut.extract(constraint_EA)

## take a spatial mean (not worrying about area-weighting but we could)
cube_pres_EA_ts = cube_pres_EA.collapsed(['longitude','latitude'], iris.analysis.MEAN)
cube_fut_EA_ts = cube_fut_EA.collapsed(['longitude','latitude'], iris.analysis.MEAN)

## calculate the year mean time series from month mean
iris.coord_categorisation.add_year(cube_pres_EA_ts, 'time')
cube_pres_ts_yr = cube_pres_EA_ts.aggregated_by('year',iris.analysis.MEAN)
iris.coord_categorisation.add_year(cube_fut_EA_ts, 'time')
cube_fut_ts_yr = cube_fut_EA_ts.aggregated_by('year',iris.analysis.MEAN)

# take a temporal mean
cube_pres_EA_2D = cube_pres_EA.collapsed(['time'], iris.analysis.MEAN)
cube_fut_EA_2D = cube_fut_EA.collapsed(['time'], iris.analysis.MEAN)

# calculate the difference
diff_EA_2D = cube_fut_EA_2D - cube_pres_EA_2D

### Long rains task
## add seasons
iris.coord_categorisation.add_season(cube_pres_EA, 'time',seasons=['JF','MAM','JJAS','OND'])
iris.coord_categorisation.add_season(cube_fut_EA, 'time',seasons=['JF','MAM','JJAS','OND'])
## make constraint
constraint_MAM = iris.Constraint(season="MAM")
## extract MAM "long rains"
cube_pres_EA_MAM = cube_pres_EA.extract(constraint_MAM)
cube_fut_EA_MAM = cube_fut_EA.extract(constraint_MAM)

## make a spatial mean of the long rains change
cube_pres_EA_ts_MAM = cube_pres_EA_MAM.collapsed(['longitude','latitude'], iris.analysis.MEAN)
cube_fut_EA_ts_MAM = cube_fut_EA_MAM.collapsed(['longitude','latitude'], iris.analysis.MEAN)

# take a temporal mean of long rains
cube_pres_EA_2D_MAM = cube_pres_EA_MAM.collapsed(['time'], iris.analysis.MEAN)
cube_fut_EA_2D_MAM = cube_fut_EA_MAM.collapsed(['time'], iris.analysis.MEAN)
# calculate the difference
diff_EA_2D_MAM = cube_fut_EA_2D_MAM - cube_pres_EA_2D_MAM


## plotting
fig = plt.figure(figsize=(12,8))
# plot of temperal change
ax1 = plt.subplot(2,2,1) ## BUG4: changed to "qplt."
qplt.plot(cube_pres_EA_ts, color='grey')
qplt.plot(cube_pres_ts_yr, color='black')
qplt.plot(cube_fut_EA_ts,color='pink')
qplt.plot(cube_fut_ts_yr,color='darkred')
plt.title('HadGEM2-ES')
plt.ylim([0,0.2])

# plot of change in spatial distribution
ax2 = plt.subplot(2,2,2)  # BUG5:changed to "1,1,2" 
qplt.contourf(diff_EA_2D, cmap=plt.cm.RdBu, levels=np.arange(-0.026,0.027,0.004), extend='both')
plt.gca().coastlines()
plt.title('HadGEM2-ES 2040s RCP8.5 - present day')

# plot of temperal change
ax3 = plt.subplot(2,2,3) ## BUG4: changed to "qplt."
qplt.plot(cube_pres_EA_ts_MAM, color='grey')
qplt.plot(cube_fut_EA_ts_MAM,color='pink')
plt.title('long rains (MAM)')
plt.ylim([0,0.2])

# plot of change in spatial distribution
ax4 = plt.subplot(2,2,4)  # BUG5:changed to "1,1,2" 
qplt.contourf(diff_EA_2D_MAM, cmap=plt.cm.RdBu, levels=np.arange(-0.026,0.027,0.004), extend='both')
plt.gca().coastlines()
plt.title('long rains (MAM)')

## SHOW
#qplt.show()

## SAVE
## in git bash use >>> start test.png
plt.savefig("day2_task1.png")
